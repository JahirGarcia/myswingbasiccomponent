package Pojo;

import java.util.List;

/**
 *
 * @author _TERIO_
 */
public class Docente implements Comparable<Docente>{
    private int Codigo;
    private String Fecha_de_Nacimiento;
    private String Nombres;
    private String Apellidos;
    private Sexo sexo;
    private EstadoCivil estadocivil;
    private GradoAcademico gradoAcademico;
    private CategoriaDocente categoriaDocente;
    private String Cedula;
    private String INSS;
    private String foto;
    private List<Asignatura> asignaturas;

    public Docente() {
    }

    public Docente(int Codigo, String Fecha_de_Nacimiento, String Nombres, String Apellidos, Sexo sexo, EstadoCivil estadocivil, GradoAcademico gradoAcademico, CategoriaDocente categoriaDocente, String Cedula, String INSS, String foto, List<Asignatura> asignaturas) {
        this.Codigo = Codigo;
        this.Fecha_de_Nacimiento = Fecha_de_Nacimiento;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.sexo = sexo;
        this.estadocivil = estadocivil;
        this.gradoAcademico = gradoAcademico;
        this.categoriaDocente = categoriaDocente;
        this.Cedula = Cedula;
        this.INSS = INSS;
        this.foto = foto;
        this.asignaturas = asignaturas;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
    
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public String getFecha_de_Nacimiento() {
        return Fecha_de_Nacimiento;
    }

    public void setFecha_de_Nacimiento(String Fecha_de_Nacimiento) {
        this.Fecha_de_Nacimiento = Fecha_de_Nacimiento;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public EstadoCivil getEstadocivil() {
        return estadocivil;
    }

    public void setEstadocivil(EstadoCivil estadocivil) {
        this.estadocivil = estadocivil;
    }

    public GradoAcademico getGradoAcademico() {
        return gradoAcademico;
    }

    public void setGradoAcademico(GradoAcademico gradoAcademico) {
        this.gradoAcademico = gradoAcademico;
    }

    public CategoriaDocente getCategoriaDocente() {
        return categoriaDocente;
    }

    public void setCategoriaDocente(CategoriaDocente categoriaDocente) {
        this.categoriaDocente = categoriaDocente;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getINSS() {
        return INSS;
    }

    public void setINSS(String INSS) {
        this.INSS = INSS;
    }
    @Override
    public int compareTo(Docente o){
        return this.Cedula.compareTo(o.getCedula());
    }
}
