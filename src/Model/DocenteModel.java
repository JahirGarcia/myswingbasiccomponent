package Model;

import Pojo.CategoriaDocente;
import Pojo.Docente;
import Pojo.EstadoCivil;
import Pojo.GradoAcademico;
import Pojo.Sexo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author _TERIO_
 */
public class DocenteModel {

    private List<Docente> docentes;

    public DocenteModel() {
        docentes = new ArrayList<>();
        populate();
    }

    public void add(Docente docente) {
        docentes.add(docente);
    }
    
    public void edit(Docente oldDocente, Docente newDocente) {
        int indice = docentes.indexOf(oldDocente);
        docentes.set(indice, newDocente);
    }

    public List<Docente> getAll() {
        return docentes;
    }
    
    public Docente findAny(Predicate<Docente> p){
        Optional<Docente> opt = docentes.stream().filter(p).findAny();        
        return opt.isPresent() ? opt.get() : null;
    }
    
    public List<Docente> findMany(Predicate<Docente> p){
        return docentes.stream().filter(p).collect(Collectors.toList());
    }
    
    private void populate(){
        Docente[] array = {
            new Docente(1, "12/02/1985", "Pepito", "Perez", 
                    Sexo.Masculino, EstadoCivil.Soltero, 
                    GradoAcademico.Doctorado, CategoriaDocente.Asistente, 
                    "001-120285-0045A", "16598-5", "pepito.jpg", new ArrayList()),
            new Docente(2, "11/05/1984", "Ana", "Conda", 
                    Sexo.Femenino, EstadoCivil.Soltero, 
                    GradoAcademico.Maestria, CategoriaDocente.AuxiliarCatedra, 
                    "001-110584-0001B", "16685-2", "ana.jpg", new ArrayList())
        };
        docentes.addAll(Arrays.asList(array));
    }
/*
    public Docente findByCedula(String cedula) {
        Docente tmp = new Docente();
        tmp.setCedula(cedula);
        int index = Collections.binarySearch(docentes, tmp, new Comparator<Docente>() {
            @Override
            public int compare(Docente o1, Docente o2) {
                return o1.getCedula().compareTo(o2.getCedula());
            }
        });

        return index < 0 ? null : docentes.get(index);
    }

    public Docente findByCodigo(int codigo) {
        return docentes.stream().filter((d) -> d.getCodigo() == codigo).findAny().get();
    }

    public Docente findByINSS(String INSS) {
        return docentes.stream().filter((d) -> d.getINSS().equalsIgnoreCase(INSS)).findAny().get();
    }

    public List<Docente> findByApeliido(String apellido) {        
        return docentes.stream()
                .filter((d) 
                        -> d.getApellidos().equalsIgnoreCase(apellido))
                .collect(Collectors.toList()); 
    }

    public Docente[] findByEstadoCivil(EstadoCivil estadoCivil) {
        Docente tmp = new Docente();
        tmp.setEstadocivil(estadoCivil);
        List<Docente> retornar = new ArrayList<>();
        for (int a = 0; a < docentes.size(); a++) {
            if (tmp.equals(docentes.get(a))) {
                retornar.add(docentes.get(a));
            }
        }
        return retornar.toArray(new Docente[retornar.size()]);
    }

    public Docente[] findByGradoAcademico(GradoAcademico gradoAcademico) {
        Docente tmp = new Docente();
        tmp.setGradoAcademico(gradoAcademico);
        List<Docente> retornar = new ArrayList<>();
        for (int a = 0; a < docentes.size(); a++) {
            if (tmp.equals(docentes.get(a))) {
                retornar.add(docentes.get(a));
            }
        }
        return retornar.toArray(new Docente[retornar.size()]);
    }

    public Docente[] findByCategoriaDocentes(CategoriaDocente categoriaDocente) {
        Docente tmp = new Docente();
        tmp.setCategoriaDocente(categoriaDocente);
        List<Docente> retornar = new ArrayList<>();
        for (int a = 0; a < docentes.size(); a++) {
            if (tmp.equals(docentes.get(a))) {
                retornar.add(docentes.get(a));
            }
        }
        return retornar.toArray(new Docente[retornar.size()]);
    }*/
}
