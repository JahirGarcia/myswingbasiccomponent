/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.DocenteModel;
import Pojo.Asignatura;
import Pojo.CategoriaDocente;
import Pojo.Docente;
import Pojo.EstadoCivil;
import Pojo.GradoAcademico;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;

/**
 *
 * @author Jadpa21
 */
public class DocenteServicio {
    private DocenteModel docenteModel;

    public DocenteServicio(DocenteModel docenteModel) {
        this.docenteModel = docenteModel;
    }
    
    public void addDocente(Docente docente){
        docenteModel.add(docente);
    }
    
    public void editDocente(Docente oldDocente, Docente newDocente) {
        docenteModel.edit(oldDocente, newDocente);
    }
    
    public List<Docente> getAllDocentes(){
        return docenteModel.getAll();
    }
    
    public ComboBoxModel getCmbModelEstadoCivil(){
        return new DefaultComboBoxModel(EstadoCivil.values());
    }
    
    public ComboBoxModel getCmbModelGradoAcademico(){
        return new DefaultComboBoxModel(GradoAcademico.values());
    }
    
    public ComboBoxModel getCmbModelCategoriaDocente(){
        return new DefaultComboBoxModel(CategoriaDocente.values());
    }
    
    public ListModel getLstModelAsignatura() {
            DefaultListModel<Asignatura> model = new DefaultListModel<>();
        
        Arrays.asList(Asignatura.values())
            .forEach(a -> model.addElement(a));
        
        return model;
    }
    
    public String saveImageProlife(File profilePicture) throws IOException {
        String profileDirPath = System.getProperty("user.dir")+"\\ProfilePicture";
        File profileDir = new File(profileDirPath);
        
        if(!profileDir.exists())
            profileDir.mkdir();
        
        Path src = profilePicture.toPath();
        Path dst = profileDir.toPath();
        Files.copy(src, dst.resolve(src.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        
        return src.getFileName().toString();
        
    }
}
